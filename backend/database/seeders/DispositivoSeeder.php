<?php

namespace Database\Seeders;

use App\Models\Dispositivo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DispositivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "disp_nombre" => "dipositivo 1",
                "mode_id" => 1,
                "bode_id"=>1
            ],
            [
                "disp_nombre" => "dipositivo 2",
                "mode_id" => 1,
                "bode_id"=>2
            ],
            [
                "disp_nombre" => "dipositivo 3",
                "mode_id" => 1,
                "bode_id"=>3
            ],
            [
                "disp_nombre" => "dipositivo 4",
                "mode_id" => 2,
                "bode_id"=>1
            ],
            [
                "disp_nombre" => "dipositivo 5",
                "mode_id" => 2,
                "bode_id"=>2
            ],
            [
                "disp_nombre" => "dipositivo 6",
                "mode_id" => 2,
                "bode_id"=>3
            ],
            [
                "disp_nombre" => "dipositivo 7",
                "mode_id" => 3,
                "bode_id"=>1
            ],
            [
                "disp_nombre" => "dipositivo 8",
                "mode_id" => 3,
                "bode_id"=>2
            ],
            [
                "disp_nombre" => "dipositivo 9",
                "mode_id" => 3,
                "bode_id"=>3
            ],
            [
                "disp_nombre" => "dipositivo 10",
                "mode_id" => 4,
                "bode_id"=>1
            ],
            [
                "disp_nombre" => "dipositivo 11",
                "mode_id" => 4,
                "bode_id"=>2
            ],
            [
                "disp_nombre" => "dipositivo 12",
                "mode_id" => 4,
                "bode_id"=>3
            ],
            [
                "disp_nombre" => "dipositivo 13",
                "mode_id" => 5,
                "bode_id"=>1
            ],
            [
                "disp_nombre" => "dipositivo 14",
                "mode_id" => 5,
                "bode_id"=>2
            ],
            [
                "disp_nombre" => "dipositivo 15",
                "mode_id" => 5,
                "bode_id"=>3
            ],
            [
                "disp_nombre" => "dipositivo 16",
                "mode_id" => 6,
                "bode_id"=>1
            ],
            [
                "disp_nombre" => "dipositivo 17",
                "mode_id" => 6,
                "bode_id"=>2
            ],
            [
                "disp_nombre" => "dipositivo 18",
                "mode_id" => 6,
                "bode_id"=>3
            ],

            
        ];


        foreach($data as $item){
            Dispositivo::create([
                "disp_nombre" => $item["disp_nombre"],
                "mode_id" =>$item["mode_id"],
                "bode_id" =>$item["bode_id"]
            ]);

        }
    }
}
