<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BodegaSeeder::class);
        $this->call(DispositivoSeeder::class);
        $this->call(ModeloSeeder::class);
        $this->call(MarcaSeeder::class);
    }
}
