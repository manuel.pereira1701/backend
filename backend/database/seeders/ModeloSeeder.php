<?php

namespace Database\Seeders;

use App\Models\Modelo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ModeloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "mode_nombre" => "Galaxy A33 5G",
                "marc_id" => 1
            ],
            [
                "mode_nombre" => "Galaxy S22",
                "marc_id" => 1
            ],
            [
                "mode_nombre" => "iPhone 13 Pro Max",
                "marc_id" => 2
            ],
            [
                "mode_nombre" => "iPhone 12 Pro",
                "marc_id" => 2
            ],
            [
                "mode_nombre" => "Redmi Note 11 Pro 5G",
                "marc_id" => 3
            ],
            [
                "mode_nombre" => "POCO X4 Pro 5G",
                "marc_id" => 3
            ]
        ];


        foreach($data as $item){
            Modelo::create([
                "mode_nombre" => $item["mode_nombre"],
                "marc_id" =>$item["marc_id"]
            ]);

        }
    }
}
