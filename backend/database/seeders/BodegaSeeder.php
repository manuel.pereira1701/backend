<?php

namespace Database\Seeders;

use App\Models\Bodega;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BodegaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "bodega 1",
            "bodega 2",
            "bodega 3",
        ];


        foreach($data as $item){
            Bodega::create([
                "bode_nombre" => $item
            ]);

        }
    }
}
