<?php

namespace Database\Seeders;

use App\Models\Marca;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MarcaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Samsung",
            "Apple",
            "Xiaomi"
        ];


        foreach($data as $item){
            Marca::create([
                "marc_nombre" => $item
            ]);

        }
    }
}
