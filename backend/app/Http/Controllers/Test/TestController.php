<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\Bodega;
use App\Models\Dispositivo;
use App\Models\Marca;
use App\Models\Modelo;
use Exception;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function listarMarcas()
    {
        try {
            $marcas = Marca::select("id", "marc_nombre as label")
                ->get();
            return $marcas;
        } catch (Exception $th) {
            return response()->json(["status" => false, "data" => $th->getMessage()], 500);
        }
    }

    public function listarModelos(Request $request)
    {
        try {
            $modelos = Modelo::select("id", "mode_nombre as label", "marc_id")
                ->with("marca:id,marc_nombre")
                ->where("marc_id", $request["marc_id"])
                ->get();

            return $modelos;
        } catch (Exception   $th) {
            return response()->json(["status" => false, "data" => $th->getMessage()], 500);
        }
    }

    public function listarDispositivo(Request $request)
    {

        try {

            $dispositivos = Dispositivo::select("id", "disp_nombre", "mode_id")
                ->with("modelo:id,mode_nombre,marc_id")
                ->when($request["mode_id"] != 0, function ($q) use ($request) {
                    return $q->where("mode_id", $request["mode_id"]);
                })
                ->when($request["marc_id"] != 0, function ($q) use ($request) {
                    return $q->whereRelation("modelo", "marc_id", $request["marc_id"]);
                })
                ->get();

            return $dispositivos;
        } catch (Exception $th) {
            return response()->json(["status" => false, "data" => $th->getMessage()], 500);
        }
    }

    public function listarBodegas()
    {
        try {
            $bodegas = Bodega::select("id", "bode_nombre as label")
                ->get();
            return $bodegas;
        } catch (Exception $th) {
            return response()->json(["status" => false, "data" => $th->getMessage()], 500);
        }
    }

    public function listarPorBodega(Request $request)
    {

        try {
            $dispositivos = Dispositivo::select("id","disp_nombre","bode_id","mode_id")
                ->with("bodega:id,bode_nombre","modelo:id,mode_nombre,marc_id","modelo.marca:id,marc_nombre")
                ->when($request["marc_id"] !=0 , function($q) use ($request){
                    return $q->whereRelation("modelo","marc_id",$request["marc_id"]);
                })
                ->when($request["mode_id"] !=0 , function($q) use ($request){
                    return $q->whereRelation("modelo","id",$request["mode_id"]);
                })
                ->when($request["bode_id"] != 0, function ($q) use ($request) {
                    return $q->where("bode_id", $request["bode_id"]);
                })
                ->get();
            return $dispositivos;
        } catch (Exception $th) {
            return response()->json(["status" => false, "data" => $th->getMessage()], 500);
        }
    }
}
