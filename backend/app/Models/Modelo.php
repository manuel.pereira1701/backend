<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    use HasFactory;

    protected $table = "modelo";
    protected $fillable = ["*"];


    public function dispositivo()
    {
        return $this->hasMany(Dispositivo::class, "mode_id");
    }

    public function marca()
    {
        return $this->belongsTo(Marca::class, "marc_id");
    }
}
