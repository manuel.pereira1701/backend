<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dispositivo extends Model
{
    use HasFactory;

    protected $table = "dispositivo";
    protected $fillable = ["*"];


    public function bodega()
    {
        return $this->belongsTo(Bodega::class, "bode_id");
    }

    public function modelo()
    {
        return $this->belongsTo(Modelo::class, "mode_id");
    }
}
