# Proyecto Laravel

## Requisitos para el proyecto
- Xampp
- Composer
## Instalar Proyecto

- clonar el proyecto con `git clone https://gitlab.com/manuel.pereira1701/backend.git`.
- instalar dependencias `composer install`.
- generar .env con lo mismo que envexample.
- instalar key `php artisan key:generate`.

## Crear Base de datos
- crear una base de datos en phpMyAdmin
- ir al .env y cambiar el nombre de la base de datos a la cual pusimos en phpMyAdmin
- usar el comando `php artisan migrate` y luego `php artisan db:seed`

Para Correr el Proyecto usaremos el comando `php artisan serve`
